/*
* Copyright 2015 Facebook, Inc.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#pragma once

#include <string>
#include <vector>

/**
 * Split a string using a character delimiter. Append the components
 * to 'result'.  If there are consecutive delimiters, this function skips
 * over all of them.
 */
void SplitStringUsing(const std::string& full, const char* delim, std::vector<std::string>* res);

/**
 * Split a string using one or more byte delimiters, presented
 * as a nul-terminated c string. Append the components to 'result'.
 * If there are consecutive delimiters, this function will return
 * corresponding empty strings.  If you want to drop the empty
 * strings, try SplitStringUsing().
 *
 * If "full" is the empty string, yields an empty string as the only value.
 */
void SplitStringAllowEmpty(const std::string& full, const char* delim, std::vector<std::string>* result);


/**
 * Split a string using a character delimiter.
 */
inline std::vector<std::string> Split(const std::string& full, const char* delim, bool skip_empty = true)
{
    std::vector<std::string> result;
    if (skip_empty) 
    {
        SplitStringUsing(full, delim, &result);
    }
    else 
    {
        SplitStringAllowEmpty(full, delim, &result);
    }
    return result;
}