// Copyright (C) 2016 ichenq@hotmail.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#include "Packet.h"
#include <string>
#include <iostream>

namespace net {

static ProtoBufferMessage* NewProtoMessage(const char* name)
{
    auto pool = google::protobuf::DescriptorPool::generated_pool();
    auto descriptor = pool->FindMessageTypeByName(name);
    if (descriptor)
    {
        auto factory = google::protobuf::MessageFactory::generated_factory();
        auto prototype = factory->GetPrototype(descriptor);
        if (prototype)
        {
            return prototype->New();
        }
    }
    return nullptr;
}

ProtoBufferMessage* ParseProtoMessage(MsgType type, const char* buffer, size_t size)
{
    auto name = GetMessageDescriptor(type);
    if (name == nullptr)
    {
        printf("message[%d] destriptor not found", type);
        return nullptr;
    }
    ProtoBufferMessage* msg = NewProtoMessage(name);
    if (msg == nullptr)
    {
        printf("message[%s] not found in namespace", name);
        return nullptr;
    }
    if (!msg->ParseFromArray(buffer, size))
    {
        delete msg;
        printf("parse message[%s] failed", name);
        return nullptr;
    }
    return msg;
}

////////////////////////////////////////////////////////////////////////

Packet::Packet()
{
}


Packet::~Packet()
{
}

ProtoBufferMessage* Packet::GetMessage()
{
    return msg_.get();
}

void Packet::SetHeader(const Header& head)
{
    memcpy(&head_, &head, sizeof(head));
}

void Packet::SetSessionID(uint32_t session)
{
    session_ = session;
}

IOBufPtr Packet::Encode()
{
    std::string payload;
    if (msg_)
    {
        msg_->SerializeToString(&payload);
    }
    IOBufPtr buf = std::make_shared<IOBuf>();
    buf->resize(sizeof(head_) + payload.size());
    memcpy(buf->data(), &head_, sizeof(head_));
    std::copy(payload.begin(), payload.end(), buf->begin()+sizeof(head_));
    return buf;
}

bool Packet::CopyMessageFrom(const ProtoBufferMessage* msg)
{
    assert(msg != nullptr);
    auto obj = msg->New();
    obj->MergeFrom(*msg);
    msg_.reset(obj);
    return true;
}

bool Packet::ParseMessage(const IOBuf& buf)
{
    ProtoBufferMessage* msg = ParseProtoMessage((MsgType)head_.command, buf.data(), buf.size());
    if (msg != nullptr)
    {
#ifdef _DEBUG
        std::string debug_output = msg->DebugString();
        std::cout << "Message " << head_.command << ": " << debug_output;
#endif
        msg_.reset(msg);
        return true;
    }
    std::cerr << "ParseProtoMessage failed" << std::endl;
    return false;
}

} // namespace net
