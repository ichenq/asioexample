// Copyright (C) 2016 ichenq@hotmail.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#include "Server.h"
#include <functional>

using namespace std::placeholders;

namespace net {

Server::Server(asio::io_service& io_service)
    : io_service_(io_service), acceptor_(io_service)
{
    next_session_ = 100;
}


Server::~Server()
{
}

void Server::Run()
{
    while (!incoming_.empty())
    {
        auto msg = incoming_.front();
        incoming_.pop();
        Dispatch(*msg);
    }
    while (!closing_.empty())
    {
        auto session = closing_.front();
        closing_.pop();
        DropDeadSessions(session);
    }
}

void Server::DropDeadSessions(SessionPtr session)
{
    session->Close();
    sessions_.erase(session->ID());
}

void Server::Start(const std::string& host, uint16_t port)
{
    auto addr = asio::ip::address::from_string(host);
    asio::ip::tcp::endpoint endpoint(addr, port);
    if (!acceptor_.is_open())
    {
        acceptor_.open(asio::ip::tcp::v4());
    }
    acceptor_.set_option(asio::socket_base::reuse_address(true));
    acceptor_.bind(endpoint);
    acceptor_.listen(asio::socket_base::max_connections);
    PostAccept();
}

void Server::PostAccept()
{
    auto sp = std::make_shared<Session>(io_service_, next_session_, &incoming_, &closing_);
    acceptor_.async_accept(sp->Socket(), std::bind(&Server::HandleAccept, this, sp, _1));
}

void Server::HandleAccept(SessionPtr sp, const std::error_code& ec)
{
    sp->StartRead();
    sessions_[sp->ID()] = sp;
    next_session_++;
    PostAccept();
}

void Server::Write(uint32_t sid, MsgType command, const ProtoBufferMessage* msg)
{
    auto session = sessions_[sid];
    if (session)
    {
        Header head;
        head.command = (uint16_t)command;
        head.length = msg->ByteSize();
        auto packet = std::make_shared<Packet>();
        packet->SetHeader(head);
        packet->CopyMessageFrom(msg);
        session->Write(packet);
    }
}

} //namespace net
