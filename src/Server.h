// Copyright (C) 2016 ichenq@hotmail.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#pragma once

#include <string>
#include <system_error>
#include <unordered_map>
#include <asio/io_service.hpp>
#include <asio/ip/tcp.hpp>
#include "MsgEmmittor.h"
#include "Packet.h"
#include "Session.h"

namespace net {

class Server : public MessageEmmittor
{
public:
    explicit Server(asio::io_service& io_service);
    ~Server();

    Server(const Server&) = delete;
    Server& operator=(const Server&) = delete;

    void Start(const std::string& host, uint16_t port);
    void Run();
    void Write(uint32_t session, MsgType command, const ProtoBufferMessage* msg);

private:
    void DropDeadSessions(SessionPtr session);
    void PostAccept();
    void HandleAccept(SessionPtr session, const std::error_code& ec);

private:
    asio::io_service&           io_service_;
    asio::ip::tcp::acceptor     acceptor_;
    PacketQueue                 incoming_;              // incoming packet queue
    std::queue<SessionPtr>      closing_;
    uint32_t                    next_session_ = 0;
    std::unordered_map<uint32_t, SessionPtr> sessions_;
};

} // namespace net
