// Copyright (C) 2016 ichenq@hotmail.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#pragma once

#include <cstdint>
#include <string>
#include <system_error>
#include <asio/ip/tcp.hpp>
#include "MsgEmmittor.h"
#include "Packet.h"

namespace net {

class Client : public MessageEmmittor
{
public:
    explicit Client(asio::io_service& io_service);
    ~Client();

    Client(const Client&) = delete;
    Client& operator = (const Client&) = delete;

    std::error_code Connect(const std::string& addr, uint16_t port);

    void StartRead();
    void Send(MsgType command, const ProtoBufferMessage* msg);
    void Close();
    void KeepAlive();
    void Run();

private:
    void Write(PacketPtr packet);
    void OnReadHeader(const std::error_code& ec, size_t bytes);
    void OnReadBody(const std::error_code& ec, size_t bytes);
    void OnWritten(const std::error_code& ec, size_t bytes, IOBufPtr packet);
    void OnKeepAlive(const std::error_code& ec);

private:
    asio::ip::tcp::socket   socket_;
    std::error_code         last_error_;
    Header                  head_;
    IOBuf                   buffer_;
    PacketQueue             incoming_;
    bool                    connected_ = false;

};

} // namespace net
