/*
* Copyright 2015 Facebook, Inc.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include "Strings.h"

// Split a string using a character delimiter. Append the components to 'result'.
//
// Note: For multi-character delimiters, this routine will split on *ANY* of
// the characters in the string, not the entire string as a single delimiter.
template <typename ITR>
static inline void SplitStringToIteratorUsing(const std::string& full, const char* delim, ITR& result)
{
    // Optimize the common case where delim is a single character.
    if (delim[0] != '\0' && delim[1] == '\0') {
        char c = delim[0];
        const char* p = full.data();
        const char* end = p + full.size();
        while (p != end) {
            if (*p == c) {
                ++p;
            }
            else {
                const char* start = p;
                while (++p != end && *p != c);
                *result++ = std::string(start, p - start);
            }
        }
        return;
    }

    std::string::size_type begin_index, end_index;
    begin_index = full.find_first_not_of(delim);
    while (begin_index != std::string::npos) {
        end_index = full.find_first_of(delim, begin_index);
        if (end_index == std::string::npos) {
            *result++ = full.substr(begin_index);
            return;
        }
        *result++ = full.substr(begin_index, (end_index - begin_index));
        begin_index = full.find_first_not_of(delim, end_index);
    }
}

void SplitStringUsing(const std::string& full, const char* delim, std::vector<std::string>* result)
{
    std::back_insert_iterator< std::vector<std::string> > it(*result);
    SplitStringToIteratorUsing(full, delim, it);
}


// Split a string using a character delimiter. Append the components
// to 'result'.  If there are consecutive delimiters, this function
// will return corresponding empty strings. The string is split into
// at most the specified number of pieces greedily. This means that the
// last piece may possibly be split further. To split into as many pieces
// as possible, specify 0 as the number of pieces.
//
// If "full" is the empty string, yields an empty string as the only value.
//
// If "pieces" is negative for some reason, it returns the whole string
// ----------------------------------------------------------------------
template <typename StringType, typename ITR>
static inline
void SplitStringToIteratorAllowEmpty(const StringType& full, const char* delim, int pieces, ITR& result)
{
    std::string::size_type begin_index, end_index;
    begin_index = 0;

    for (int i = 0; (i < pieces - 1) || (pieces == 0); i++) {
        end_index = full.find_first_of(delim, begin_index);
        if (end_index == std::string::npos) {
            *result++ = full.substr(begin_index);
            return;
        }
        *result++ = full.substr(begin_index, (end_index - begin_index));
        begin_index = end_index + 1;
    }
    *result++ = full.substr(begin_index);
}

void SplitStringAllowEmpty(const std::string& full, const char* delim, std::vector<std::string>* result)
{
    std::back_insert_iterator<std::vector<std::string> > it(*result);
    SplitStringToIteratorAllowEmpty(full, delim, 0, it);
}
