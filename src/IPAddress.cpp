// Copyright (C) 2016 ichenq@hotmail.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#include "IPAddress.h"
#include <iostream>
#include <asio/ip/address.hpp>
#include <asio/ip/tcp.hpp>
#include "Strings.h"

namespace net {


std::error_code ResolveDomainIP(asio::io_service& io_service, const std::string& protocol, const std::string& service, std::vector<IPAddress>* iplist)
{
    assert(iplist);
    std::error_code ec;
    asio::ip::tcp::resolver resolver(io_service);
    asio::ip::tcp::resolver::query query(protocol, service);
    asio::ip::tcp::resolver::iterator end;
    asio::ip::tcp::resolver::iterator iter = resolver.resolve(query, ec);
    if (!ec)
    {
        while (iter != end)
        {
            asio::ip::tcp::endpoint endpoint = *iter++;
            asio::ip::address addr = endpoint.address();
            iplist->push_back(std::make_pair(addr.to_string(), endpoint.port()));
        }
    }
    return ec;
}


// Is IP address or domain name
static bool IsIPAddress(const std::string& ip)
{
    std::error_code ec;
    asio::ip::address::from_string(ip, ec);
    return ec.value() == 0;
}

// Algorithm taken from https://github.com/golang/go/blob/master/src/net/ipsock.go
ErrorStatus SplitHostPort(const std::string& hostport, std::string* host, std::string* port)
{
    size_t j = 0;
    size_t k = 0;
    size_t i = hostport.rfind(':');
    if (i == std::string::npos)
    {
        *host = hostport;
        *port = "80";         // default HTTP port
        return ErrOK;
    }
    if (hostport[0] == '[')
    {
        size_t end = hostport.find(']'); // Expect the first ']' just before the last ':'.
        if (end == std::string::npos)
        {
            return ErrAddrErr; // missing ']' in address
        }
        if (end + 1 == hostport.size())
        {
            return ErrMissingPort; // There can't be a ':' behind the ']' now.
        }
        else if (end + 1 == i) // The expected result.
        {
        }
        else
        {
            // Either ']' isn't followed by a colon, or it is
            // followed by a colon that is not the last one.
            if (hostport[end + 1] == ':')
                return ErrTooManyColons;
            return ErrMissingPort;
        }
        *host = hostport.substr(1, end);
        j = 1;
        k = end + 1; // there can't be a '[' resp. ']' before these positions
    }
    else
    {
        *host = hostport.substr(0, i);
        if (host->find(':') != std::string::npos)
        {
            return ErrTooManyColons;
        }
        if (host->find('%') != std::string::npos)
        {
            return ErrMissingBrackets;
        }
    }
    if (hostport.find('[', j) != std::string::npos)
    {
        return ErrAddrErr; // Unexpected '[' in address
    }
    if (hostport.find(']', k) != std::string::npos)
    {
        return ErrAddrErr; // Unexpected ']' in address
    }
    *port = hostport.substr(i + 1);
    return ErrOK;
}

std::error_code ParseOneAddress(asio::io_service& io_service, const std::string& address, std::vector<IPAddress>* iplist)
{
    std::string host;
    std::string port;
    if (SplitHostPort(address, &host, &port) != ErrOK)
    {
        return asio::error::invalid_argument;
    }
    if (IsIPAddress(host))
    {
        IPAddress ip = std::make_pair(host, (uint16_t)atoi(port.c_str()));
        iplist->push_back(ip);
        return std::error_code();
    }
    else
    {
        return ResolveDomainIP(io_service, host, port, iplist);
    }
}

void ParseHostAddress(asio::io_service& io_service, const std::string& addresses, std::vector<IPAddress>* iplist)
{
    std::vector<std::string> addrlist = Split(addresses, ",");
    for (auto addr : addrlist)
    {
        std::vector<IPAddress> vec;
        auto ec = ParseOneAddress(io_service, addr, &vec);
        if (!ec)
        {
            std::copy(vec.begin(), vec.end(), std::back_inserter(*iplist));
        }
        else
        {
            std::cerr << "ParseHostAddress, " << addr << ", " << ec.value() << ": " << ec.message() << std::endl;
        }
    }
}

} // namespace net
