// Copyright (C) 2016 ichenq@hotmail.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#pragma once

#include <cstdint>
#include <queue>
#include <vector>
#include <memory>
#include <google/protobuf/message.h>
#include "MsgFactory.h"

namespace net {

typedef ::google::protobuf::Message ProtoBufferMessage;

typedef std::vector<char>           IOBuf;
typedef std::shared_ptr<IOBuf>      IOBufPtr;


ProtoBufferMessage* ParseProtoMessage(MsgType type, const char* buffer, size_t size);


struct Header
{
    uint32_t length = 0;
    uint32_t command = 0;
};


// Packet class
class Packet
{
public:
    Packet();
    ~Packet();

    uint32_t GetCommand() const { return head_.command; }

    uint32_t Session() const { return session_; }

    ProtoBufferMessage* GetMessage();

    void SetHeader(const Header& head);
    void SetSessionID(uint32_t session);

    IOBufPtr Encode();

    bool Packet::CopyMessageFrom(const ProtoBufferMessage* msg);

    bool ParseMessage(const IOBuf& buffer);

private:
    Header      head_;
    uint32_t    session_ = 0;
    std::unique_ptr<ProtoBufferMessage> msg_;
};

typedef std::shared_ptr<Packet> PacketPtr;
typedef std::queue<PacketPtr>   PacketQueue;

} // namespace net
