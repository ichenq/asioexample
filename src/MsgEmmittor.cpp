// Copyright (C) 2016 ichenq@hotmail.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#include "MsgEmmittor.h"
#include <exception>
#include <iostream>
#include <typeinfo>


namespace net {

MessageEmmittor::MessageEmmittor()
{
}


MessageEmmittor::~MessageEmmittor()
{
}


void MessageEmmittor::RegMsgCallback(uint16_t msg, MessageCallback cb)
{
    handlers_[msg] = cb;
}


void MessageEmmittor::Dispatch(Packet& packet)
{
    uint32_t msg = packet.GetCommand();
    auto cb = handlers_[msg];
    if (cb)
    {
        try 
        {
            cb(packet);
        }
        catch (std::exception& ex)
        {
            std::cerr << typeid(ex).name() << ": " << ex.what() << std::endl;
        }
    }
}

} // namespace net
