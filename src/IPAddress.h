// Copyright (C) 2016 ichenq@hotmail.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#pragma once

#include <cstdint>
#include <utility>
#include <string>
#include <vector>
#include <system_error>
#include <asio/io_service.hpp>

namespace net {

typedef std::pair<std::string, uint16_t> IPAddress;

// error code of SplitHostPort
enum ErrorStatus
{
    ErrOK,
    ErrMissingPort,
    ErrTooManyColons,
    ErrMissingBrackets,
    ErrAddrErr,
};


// DSN resolve, resolve domain name to IP address list
std::error_code ResolveDomainIP(asio::io_service& io_service, 
                                const std::string& protocol, 
                                const std::string& service, 
                                std::vector<IPAddress>* iplist);


// Splits a network address of the form "host:port", "[host]:port" 
// or "[ipv6-host%zone]:port" into host or ipv6-host%zone and port.  
// A literal address or host name for IPv6 must be enclosed in square brackets, 
// as in "[::1]:80", "[ipv6-host]:http" or "[ipv6-host%zone]:80".
ErrorStatus SplitHostPort(const std::string& hostport, std::string* host, std::string* port);


// Parse host address to `IPAddress` objects
// `address` can be domain name list eg. "patch.n.m.youzu.com, bd.sg.uuzu.com",
// or IP list eg. "122.226.211.154, 122.226.211.155:8080",
// or combined eg. "patch.n.m.youzu.com:8080, 122.226.211.154",
void ParseHostAddress(asio::io_service& io_service, const std::string& addresses, std::vector<IPAddress>* iplist);

} // namespace net
