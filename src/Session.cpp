// Copyright (C) 2016 ichenq@hotmail.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#include "Session.h"
#include <iostream>
#include <functional>
#include <asio.hpp>

using namespace std::placeholders;

namespace net {

Session::Session(asio::io_service& io_service, uint32_t session, PacketQueue* incoming, std::queue<SessionPtr>* signal)
    : socket_(io_service), session_(session), incoming_(incoming), error_signal_(signal)
{
    buffer_.reserve(256);
}


Session::~Session()
{
}

void Session::Close()
{
    shutdown_ = true;
    incoming_ = nullptr;
    error_signal_ = nullptr;
}

void Session::StartRead()
{
    memset(&head_, 0, sizeof(head_));
    asio::async_read(socket_, asio::buffer(&head_, sizeof(head_)),
        std::bind(&Session::HandleReadHeader, this, _1, _2));
}

void Session::HandleError(const std::error_code& ec)
{
    if (!shutdown_)
    {
        last_error_ = ec;
        if (error_signal_)
        {
            error_signal_->push(shared_from_this());
        }
    }
}

void Session::HandleReadHeader(const std::error_code& ec, size_t bytes)
{
    if (ec)
    {
        std::cerr << session_ << ": " << ec.message() << std::endl;
        HandleError(ec);
        return;
    }
    if (head_.length > 0)
    {
        buffer_.resize(head_.length);
        asio::async_read(socket_, asio::buffer(buffer_.data(), head_.length),
            std::bind(&Session::HandleReadBody, this, _1, _2));
    }
    else
    {
        StartRead();
    }
}

void Session::HandleReadBody(const std::error_code& ec, size_t bytes)
{
    if (ec)
    {
        std::cerr << session_ << ": " << ec.message() << std::endl;
        HandleError(ec);
        return;
    }
    if (incoming_ != nullptr)
    {
        auto packet = std::make_shared<Packet>();
        packet->SetHeader(head_);
        packet->SetSessionID(session_);
        packet->ParseMessage(buffer_);
        incoming_->push(packet);
    }
    StartRead();
}

void Session::Write(PacketPtr packet)
{
    auto buf = packet->Encode();
    asio::async_write(socket_,
        asio::buffer(buf->data(), buf->size()),
        std::bind(&Session::HandleWrite, this, _1, _2, buf));
}

void Session::HandleWrite(const std::error_code& ec, size_t bytes, IOBufPtr buf)
{
    if (ec)
    {
        std::cerr << "Session::HandleWrite: " << ec.value() << ", " << ec.message() << std::endl;
        HandleError(ec);
    }
}

} //namespace net 
