// Copyright (C) 2016 ichenq@hotmail.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#pragma once

#include <cstdint>
#include <functional>
#include <unordered_map>
#include "Packet.h"

namespace net {

typedef std::function<void(Packet& packet)> MessageCallback;

class MessageEmmittor
{
public:
    MessageEmmittor();
    ~MessageEmmittor();

    MessageEmmittor(const MessageEmmittor&) = delete;
    MessageEmmittor& operator=(const MessageEmmittor&) = delete;

    void RegMsgCallback(uint16_t msg, MessageCallback cb);
    void Dispatch(Packet& packet);

private:
    std::unordered_map<uint32_t, MessageCallback> handlers_;
};

} //namespace net
