// Copyright (C) 2016 ichenq@hotmail.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#include "MsgFactory.h"

const char* GetMessageTypeName(MsgType type)
{
    switch (type)
    {
#define XX(msg, a, b) case msg: return #msg;
        GEN_MESSAGE_MAP(XX)
#undef XX
    };
    return "unkonwn";
}

const char* GetMessageDescriptor(MsgType type)
{
    switch (type) 
    {
#define XX(msg, _, name) case msg: return name;
        GEN_MESSAGE_MAP(XX)
#undef XX
    };
    return nullptr;
}
