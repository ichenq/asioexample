// Copyright (C) 2016 ichenq@hotmail.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#include "Client.h"
#include <iostream>
#include <functional>
#include <asio.hpp>

using namespace std::placeholders;

namespace net {

Client::Client(asio::io_service& io_service)
    : socket_(io_service)
{

}

Client::~Client()
{

}

void Client::Run()
{
    while (!incoming_.empty())
    {
        auto msg = incoming_.front();
        incoming_.pop();
        Dispatch(*msg);
    }
}

std::error_code Client::Connect(const std::string& addr, uint16_t port)
{
    std::error_code ec;
    auto address = asio::ip::address::from_string(addr, ec);
    if (!ec)
    {
        asio::ip::tcp::endpoint endpoint(address, port);
        socket_.connect(endpoint, ec);
        if (!ec)
        {
            asio::socket_base::linger option(true, 0);
            socket_.set_option(option);
            connected_ = true;
        }
    }
    return ec;
}

void Client::Send(MsgType command, const ProtoBufferMessage* msg)
{
    Header head;
    head.command = (uint16_t)command;
    head.length = msg->ByteSize();
    auto packet = std::make_shared<Packet>();
    packet->SetHeader(head);
    packet->CopyMessageFrom(msg);
    Write(packet);
}


void Client::StartRead()
{
    memset(&head_, 0, sizeof(head_));
    asio::async_read(socket_, asio::buffer(&head_, sizeof(head_)),
        std::bind(&Client::OnReadHeader, this, _1, _2));
}


void Client::Close()
{
}

void Client::Write(PacketPtr packet)
{
    auto buf = packet->Encode();
    asio::async_write(socket_,
        asio::buffer(buf->data(), buf->size()),
        std::bind(&Client::OnWritten, this, _1, _2, buf));
}

void Client::KeepAlive()
{

}

void Client::OnReadHeader(const std::error_code& ec, size_t bytes)
{
    if (ec)
    {
        std::cerr << ec.message() << std::endl;
        return;
    }
    last_error_ = ec;
    if (head_.length > 0)
    {
        buffer_.resize(head_.length);
        asio::async_read(socket_, asio::buffer(buffer_.data(), head_.length),
            std::bind(&Client::OnReadBody, this, _1, _2));
    }
    else
    {
        StartRead();
    }
}

void Client::OnReadBody(const std::error_code& ec, size_t bytes)
{
    if (ec)
    {
        std::cerr << ec.message() << std::endl;
        return;
    }
    last_error_ = ec;
    auto packet = std::make_shared<Packet>();
    packet->SetHeader(head_);
    packet->ParseMessage(buffer_);
    incoming_.push(packet);
    StartRead();
}

void Client::OnWritten(const std::error_code& ec, size_t bytes, IOBufPtr packet)
{
    if (ec)
    {
        std::cerr << "Session::HandleWrite: " << ec.value() << ", " << ec.message() << std::endl;
    }
    last_error_ = ec;
}

void Client::OnKeepAlive(const std::error_code& ec)
{

}

} //namespace net