// Copyright (C) 2016 ichenq@hotmail.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#pragma once

#include <cstdint>
#include <memory>
#include <system_error>
#include <asio/io_service.hpp>
#include <asio/ip/tcp.hpp>
#include "Packet.h"

namespace net {

class Session;
typedef std::shared_ptr<Session>    SessionPtr;

class Session : public std::enable_shared_from_this<Session>
{
public:
    Session(asio::io_service& io_service, uint32_t session, PacketQueue* incoming, std::queue<SessionPtr>* signal);
    ~Session();

    Session(const Session&) = delete;
    Session& operator=(const Session&) = delete;

    uint32_t ID() const { return session_; }
    asio::ip::tcp::socket& Socket() { return socket_; }

    void StartRead();
    void Write(PacketPtr packet);

    void Close();

    std::error_code LastError() { return last_error_; }

private:
    void HandleError(const std::error_code& ec);
    void HandleReadHeader(const std::error_code& ec, size_t bytes);
    void HandleReadBody(const std::error_code& ec, size_t bytes);
    void HandleWrite(const std::error_code& ec, size_t bytes, IOBufPtr buf);

private:
    asio::ip::tcp::socket   socket_;
    const uint32_t          session_;
    std::error_code         last_error_;
    Header                  head_;
    IOBuf                   buffer_;
    PacketQueue*            incoming_;      // incoming packet queue
    std::queue<SessionPtr>* error_signal_;
    bool                    shutdown_ = false;
};

} // namespace net
