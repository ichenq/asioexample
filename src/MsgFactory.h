﻿// Copyright (C) 2016 ichenq@hotmail.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#pragma once

#include <stdint.h>

#define GEN_MESSAGE_MAP(XX) \
    XX(CM_LOGIN,                    2001, "proto.LoginReq"                      ) /* 登陆获取网关 */	\
    XX(SM_LOGIN_STATUS,             2002, "proto.LoginRes"                      ) /* 登陆返回 */	\

enum MsgType : uint16_t {
#define XX(msg, v, _) msg = v,
    GEN_MESSAGE_MAP(XX)
#undef XX
};


// enum to its string represent
const char* GetMessageTypeName(MsgType type);

// enum to its protobuf message descriptor
const char* GetMessageDescriptor(MsgType type);
