// Copyright (C) 2016 ichenq@hotmail.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

#include <iostream>
#include <exception>
#include <typeinfo>
#include <thread>
#include <chrono>
#include "Client.h"
#include "IPAddress.h"
#include "MsgFactory.h"
#include "proto/login.pb.h"


asio::io_service io_service;
net::Client client(io_service);



void handleLoginResult(net::Packet& packet)
{
    const auto res = (proto::LoginRes*)(packet.GetMessage());
    assert(res != nullptr);

    // login result
}

void startLogin()
{
    proto::LoginReq req;
    req.set_username("test001");
    req.set_token("unIJli7fR7nLQExqgVlUyLUTIyxJolNpHHxA6SqR8CEBMJbC");
    client.Send(CM_LOGIN, &req);
}

int main(int argc, char* argv[])
{
    if (argc < 2)
    {
        // host format: 127.0.0.1:12345
        std::cerr << "Usage: client_demo [host]" << std::endl;
        return 1;
    }

    std::vector<net::IPAddress> iplist;
    net::ParseHostAddress(io_service, argv[1], &iplist);
    for (auto& ip : iplist)
    {
        auto ec = client.Connect(ip.first, ip.second);
        if (ec)
        {
            std::cerr << "Connect " << ip.first << ": " << ec.message() << std::endl;
            return 1;
        }
        break;
    }

    client.RegMsgCallback(SM_LOGIN_STATUS, handleLoginResult);
    client.StartRead();

    // send login request
    startLogin();

    try
    {
        while (true)
        {
            io_service.poll();
            client.Run();
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
    }
    catch (std::exception& ex)
    {
        std::cerr << typeid(ex).name() << ": " << ex.what() << std::endl;
        return 1;
    }

    return 0;
}